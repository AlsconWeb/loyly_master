/* ===================================
 sticky nav
 ====================================== */

jQuery( window ).scroll( function( $ ) {
	if ( jQuery( document ).scrollTop() > 50 ) {
		jQuery( 'nav' ).addClass( 'shrink' );
	} else {
		jQuery( 'nav' ).removeClass( 'shrink' );
	}
} );

jQuery( document ).ready( function( $ ) {
	/*Testimonials Slider Start*/
	$('.testimonials-slider').slick({
		arrows:false,
		dots:true,
	});
	/*Testimonials Slider End*/
	/*Delete empty tag <p> Start*/
	$('p').filter(function(){
	   return this.innerHTML == '&nbsp;';
	}).remove();
	/*Delete empty tag <p> End*/
	$('.navbar-toggle').click(function(){
		$(this).toggleClass('close');
		$('.navbar-collapse.collapse').toggleClass('open');
		$('body').toggleClass('modal-open');
	});
	//Disable mouse scroll wheel zoom on embedded Google Maps
	jQuery( '.maps' ).click( function() {
		jQuery( '.maps iframe' ).css( 'pointer-events', 'auto' );
	} );

	jQuery( '.maps' ).mouseleave( function() {
		jQuery( '.maps iframe' ).css( 'pointer-events', 'none' );
	} );


	/* ===================================
	 shrink navigation
	 ====================================== */

	jQuery( window ).scroll();
	jQuery( window ).scroll( function() {
		if ( jQuery( window )
			.scrollTop() > 10 ) {
			jQuery( 'nav' )
				.addClass( 'shrink' );
		} else {
			jQuery( 'nav' )
				.removeClass( 'shrink' );
		}
	} );

	jQuery( '.navigation-menu' )
		.onePageNav( {
			scrollSpeed: 750,
			scrollThreshold: 0.2, // Adjust if Navigation highlights too early or too late
			scrollOffset: 79, //Height of Navigation Bar
			currentClass: 'active',
			filter: ':not(.btn-very-small)'
		} );

	setTimeout( function() {
		jQuery( window ).scroll();
	}, 500 );

	//close navbar menu after clicking menu href
	jQuery( 'ul.navbar-nav li a' )
		.click( function( e ) {
			jQuery( this )
				.parents( 'div.navbar-collapse' )
				.removeClass( 'in' );
		} );

	// pull-menu close on href click event in mobile devices
	jQuery( '.pull-menu a.section-link' )
		.click( function( e ) {
			if ( jQuery( window ).width() <= 500 )
				jQuery( '#close-button' ).click();
		} );

	/*==============================================================
	 smooth scroll
	 ==============================================================*/
	var hash = window.location.hash.substr( 1 );
	if ( hash != '' ) {
		var scrollAnimationTime = 1200,
			scrollAnimation = 'easeInOutExpo';

		var target = '#' + hash;
		jQuery( 'html, body' ).stop()
			.animate( {
				'scrollTop': jQuery( target )
					.offset()
					.top
			}, scrollAnimationTime, scrollAnimation, function() {
				window.location.hash = target;
			} );
	}


	var scrollAnimationTime = 1200,
		scrollAnimation = 'easeInOutExpo';
	jQuery( 'a.scrollto' ).bind( 'click.smoothscroll', function( event ) {
		event.preventDefault();
		var target = this.hash;
		jQuery( 'html, body' ).stop()
			.animate( {
				'scrollTop': jQuery( target )
					.offset()
					.top
			}, scrollAnimationTime, scrollAnimation, function() {
				window.location.hash = target;
			} );
	} );

	// Inner links
	jQuery( '.inner-link' ).smoothScroll( {
		speed: 900,
		offset: -59
	} );
	jQuery( '.section-link' ).smoothScroll( {
		speed: 900,
		offset: 1
	} );

	/*==============================================================
	 toggles
	 ==============================================================*/

	jQuery( '.toggles-style2 .collapse' ).on( 'show.bs.collapse', function() {
		var id = jQuery( this ).attr( 'id' );
		jQuery( 'a[href="#' + id + '"]' ).closest( '.panel-heading' ).addClass( 'active-accordion' );
		jQuery( 'a[href="#' + id + '"] .panel-title span' ).html( '<i class="fa fa-angle-up"></i>' );
	} );
	jQuery( '.toggles-style2 .collapse' ).on( 'hide.bs.collapse', function() {
		var id = jQuery( this ).attr( 'id' );
		jQuery( 'a[href="#' + id + '"]' ).closest( '.panel-heading' ).removeClass( 'active-accordion' );
		jQuery( 'a[href="#' + id + '"] .panel-title span' ).html( '<i class="fa fa-angle-down"></i>' );
	} );
	
	//end ready
} );

/*==============================================================*/
//Reload popup OwlCarousel
/*==============================================================*/

function ReloadOwlCarousel() {
	jQuery( '.popup-main .owl-slider-full' ).owlCarousel( {
		navigation: true, // Show next and prev buttons
		slideSpeed: 300,
		paginationSpeed: 400,
		singleItem: true,
		navigationText: [ '<i class=\'fa fa-angle-left\'></i>', '<i class=\'fa fa-angle-right\'></i>' ]
	} );

	//Stop Closing magnificPopup on selected elements - START CODE

	jQuery( '.popup-main .owl-slider-full' ).click( function( e ) {
		if ( jQuery( e.target ).is( '.mfp-close' ) )
			return;
		return false;
	} );

}

/*==============================================================
 full screen
 ==============================================================*/

function SetResizeContent() {
	let minheight = jQuery( window ).height();
	jQuery( '.full-screen' ).css( 'min-height', minheight );
}

SetResizeContent();

jQuery( window ).resize( function() {
	SetResizeContent();
} );


/*==============================================================
 counter
 ==============================================================*/

jQuery( function( jQuery ) {
	// start all the timers
	animatecounters();
} );

function animatecounters() {

	jQuery( '.timer' ).each( count );

	function count( options ) {
		var jQuerythis = jQuery( this );
		options = jQuery.extend( {}, options || {}, jQuerythis.data( 'countToOptions' ) || {} );
		jQuerythis.countTo( options );
	}

}

/*==============================================================
 elements active class
 ==============================================================*/

jQuery( function( jQuery ) {
	jQuery( 'div.widget-body ul.category-list li a' ).click( function( e ) {
		jQuery( 'div.widget-body ul.category-list li a' ).removeClass( 'active' );
		jQuery( this ).addClass( 'active' );
	} );
} );

/*==============================================================
 wow animation - on scroll
 ==============================================================*/

var wow = new WOW( {
	boxClass: 'wow',
	animateClass: 'animated',
	offset: 90,
	mobile: false,
	live: true
} );
wow.init();

/*==============================================================
 ajax aagnific popup for onepage portfolio
 ==============================================================*/

jQuery( '.work-details-popup' ).on( 'click', function() {
	jQuery.magnificPopup.open( {
		items: {
			src: jQuery( this ).parents( 'li' ).find( '.popup-main' ),
		},
		type: 'inline',
		fixedContentPos: true,
		closeOnContentClick: true,
		callbacks: {
			beforeOpen: function() {
				startWindowScroll = jQuery( window ).scrollTop();
			},
			open: function() {
				jQuery( '.mfp-wrap' ).addClass( 'popup-bg' );
				ReloadOwlCarousel();
			},
			close: function() {
				jQuery( '.mfp-wrap' ).removeClass( 'popup-bg' );
				jQuery( window ).scrollTop( startWindowScroll );
			}
		}
	} );

} );

/*==============================================================
 pull menu
 ==============================================================*/

function bindEvent( el, eventName, eventHandler ) {
	if ( el.addEventListener ) {
		el.addEventListener( eventName, eventHandler, false );
	} else if ( el.attachEvent ) {
		el.attachEvent( 'on' + eventName, eventHandler );
	}
}

( function() {

	var bodyEl = document.body,
		//content = document.querySelector( '.content-wrap' ),
		openbtn = document.getElementById( 'open-button' ),
		closebtn = document.getElementById( 'close-button' ),
		isOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		if ( openbtn ) {
			bindEvent( openbtn, 'click', toggleMenu );

		}
		//openbtn.addEventListener( 'click', toggleMenu );
		if ( closebtn ) {

			bindEvent( closebtn, 'click', toggleMenu );
			//closebtn.addEventListener( 'click', toggleMenu );
		}

		// close the menu element if the target itÂ´s not the menu element or one of its descendants..

	}

	function toggleMenu() {

		if ( isOpen ) {
			classie.remove( bodyEl, 'show-menu' );
		} else {
			classie.add( bodyEl, 'show-menu' );
		}
		isOpen = ! isOpen;
	}

	init();

} )();

/*==============================================================
 countdown timer
 ==============================================================*/

jQuery( 'body' ).on( 'touchstart click', function( e ) {
	if ( jQuery( window ).width() < 992 ) {
	}
} );

/*==============================================================
 scroll to top
 ==============================================================*/
jQuery( window ).scroll( function() {
	if ( jQuery( this )
		.scrollTop() > 100 ) {
		jQuery( '.scrollToTop' )
			.fadeIn();
	} else {
		jQuery( '.scrollToTop' )
			.fadeOut();
	}
} );

//Click event to scroll to top
jQuery( '.scrollToTop' ).click( function() {
	jQuery( 'html, body' ).animate( {
		scrollTop: 0
	}, 1000 );
	return false;
} );

jQuery( '.panel a, .nav-tabs a' ).click( function( e ) {
	if ( jQuery( this ).is( '[data-parent]' ) || jQuery( this ).is( '[data-toggle]' ) ) {
		e.preventDefault();
	}

} );

/*==============================================================
 dropdown menu
 ==============================================================*/

jQuery( function() {
	jQuery( '.dropdown' ).hover(
		function() {
			jQuery( '.dropdown-menu', this ).stop( true, true ).fadeIn( 'fast' );
			jQuery( this ).toggleClass( 'open' );
			jQuery( 'b', this ).toggleClass( 'caret caret-up' );
		},
		function() {
			jQuery( '.dropdown-menu', this ).stop( true, true ).fadeOut( 'fast' );
			jQuery( this ).toggleClass( 'open' );
			jQuery( 'b', this ).toggleClass( 'caret caret-up' );
		}
	);
} );
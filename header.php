<?php
/**
 * Created 02.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP
 */

$logoWhite = get_theme_mod( 'loyly_logo_white' );
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1"/>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700&display=swap" rel="stylesheet"> 
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- navigation -->
<nav class="navbar no-margin-bottom alt-font without-button">
	<div class="container navigation-menu">
		<div class="row">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="col-lg-2 col-md-2 navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand inner-link" href="<?php bloginfo( 'url' ); ?>">
					<?php if ( $logoWhite ) : ?>
						<img
								src="<?php echo esc_url( $logoWhite ); ?>"
								alt="<?php esc_attr_e( 'Logo White', 'iwp' ); ?>"
						/>
					<?php endif; ?>
				</a>
			</div>
			<div class="col-lg-10 col-md-9 col-sm-9 collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<?php if ( has_nav_menu( 'header_menu' ) ) : ?>
					<?php
					wp_nav_menu(
						[
							'container'      => '',
							'menu_class'     => 'nav navbar-nav',
							'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'          => 0,
							'walker'         => '',
							'theme_location' => 'header_menu',
						]
					);
					?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</nav>
<!-- end navigation -->

<?php
/**
 * Created 02.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP
 */

namespace IWP;

use IWP\Customize\CustomizeTheme;
use IWP\Elementor\RegisterWidgetsElementary;

/**
 * ThemeInit File.
 */
class ThemeInit {
	/**
	 * ThemeInit construct.
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'init' ] );
		add_action( 'after_setup_theme', [ $this, 'addMenuNav' ] );
	}

	/**
	 * Init theme Loyly Master.
	 */
	public function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'addScriptAndStyle' ] );
		add_action( 'customize_register', [ $this, 'addCustomize' ] );
		RegisterWidgetsElementary::get_instance();
	}

	/**
	 * Add Customize.
	 */
	public function addCustomize(): void {
		new CustomizeTheme();
	}
	
	/**
	 * Add Script and Style Front-end.
	 */
	public function addScriptAndStyle(): void {
		// add Script.
		wp_enqueue_script( 'smooth-scroll', IWP_THEME_URL . '/assets/js/smooth-scroll.js', [ 'jquery' ], self::versionFile( '/assets/js/smooth-scroll.js' ), true );
		wp_enqueue_script( 'wow', IWP_THEME_URL . '/assets/js/wow.min.js', [ 'jquery' ], self::versionFile( '/assets/js/wow.min.js' ), true );
		wp_enqueue_script( 'slick', IWP_THEME_URL . '/assets/js/slick.min.js', [ 'main' ], self::versionFile( '/assets/js/slick.min.js' ), true );
		wp_enqueue_script( 'page-scroll', IWP_THEME_URL . '/assets/js/page-scroll.js', [ 'wow' ], self::versionFile( '/assets/js/page-scroll.js' ), true );
		wp_enqueue_script(
			'jquery-nav',
			IWP_THEME_URL . '/assets/js/jquery.nav.js',
			[ 'smooth-scroll' ],
			self::versionFile( '/assets/js/jquery.nav.js' ),
			true
		);
		wp_enqueue_script(
			'main',
			IWP_THEME_URL . '/assets/js/main.js',
			[
				'jquery',
				'smooth-scroll',
				'wow',
			],
			self::versionFile( '/assets/js/main.js' ),
			true
		);

		// add Style.
		wp_enqueue_style( 'bootstrap', IWP_THEME_URL . '/assets/css/bootstrap.min.css', '', self::versionFile( '/assets/css/bootstrap.min.css' ) );
		wp_enqueue_style( 'slick-theme', IWP_THEME_URL . '/assets/css/slick-theme.css', '', self::versionFile( '/assets/css/slick-theme.css' ) );
		wp_enqueue_style( 'slick.css', IWP_THEME_URL . '/assets/css/slick.css', '', self::versionFile( '/assets/css/slick.css' ) );
		wp_enqueue_style( 'style-theme', IWP_THEME_URL . '/style.css', [], self::versionFile( '/style.css' ) );
		wp_enqueue_style( 'font-awesome', IWP_THEME_URL . '/assets/css/font-awesome.min.css', '', self::versionFile( '/assets/css/font-awesome.min.css' ) );

		// add image sizes.
		add_image_size( 'about-image', 900, 567, true );
		add_image_size( 'product-image', 182, 182, false );
	}

	/**
	 * Return Version file.
	 *
	 * @param string $src The relative path to the file in the theme.
	 *
	 * @return false|int
	 */
	private static function versionFile( string $src ) {
		return filemtime( IWP_THEME_DIR . $src );
	}

	/**
	 * Add Menu support.
	 */
	public function addMenuNav(): void {
		register_nav_menus(
			[
				'header_menu' => __( 'Main menu', 'iwp' ),
				'footer_menu' => __( 'Footer menu', 'iwp' ),
			]
		);
	}
	
}

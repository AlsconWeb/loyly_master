<?php
/**
 * Created 04.08.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Customize
 */

namespace IWP\Customize;

/**
 * Class CustomizeTheme.
 */
class CustomizeTheme {
	/**
	 * Connect to WP_Customize_Manager class.
	 *
	 * @var \WP_Customize_Manager
	 */
	private $customize;

	/**
	 * Constructor CustomizeTheme class.
	 */
	public function __construct() {
		global $wp_customize;

		$this->customize = $wp_customize;

		$this->addSection();

		$this->generalSettings();
	}

	/**
	 * Add Section to Customize.
	 */
	public function addSection(): void {
		$this->customize->add_section(
			'general',
			[
				'title'    => __( 'General', 'iwp' ),
				'priority' => 70,
			]
		);
	}

	/**
	 * Add General Settings Field.
	 */
	public function generalSettings(): void {

		// Logo Start.
		$this->customize->add_setting(
			'loyly_logo',
			[
				'transport' => 'refresh',
				'height'    => 325,
			]
		);
		$this->customize->add_setting(
			'loyly_logo_white',
			[
				'transport' => 'refresh',
				'height'    => 325,
			]
		);

		$this->customize->add_control(
			new \WP_Customize_Image_Control(
				$this->customize,
				'loyly_logo',
				[
					'label'    => __( 'Logo', 'iwp' ),
					'section'  => 'general',
					'settings' => 'loyly_logo',
				]
			)
		);
		$this->customize->add_control(
			new \WP_Customize_Image_Control(
				$this->customize,
				'loyly_logo_white',
				[
					'label'    => __( 'Logo White', 'iwp' ),
					'section'  => 'general',
					'settings' => 'loyly_logo_white',
				]
			)
		);
		// Logo End.

		// Copyright.
		$this->customize->add_setting(
			'loyly_copyright',
			[
				'transport' => 'refresh',
				'height'    => 325,
			]
		);
		$this->customize->add_control(
			'loyly_copyright',
			[
				'section' => 'general',
				'label'   => __( 'Copyright', 'iwp' ),
				'type'    => 'textarea',
			]
		);
	}
}

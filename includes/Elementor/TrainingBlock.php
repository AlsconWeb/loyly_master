<?php
/**
 * Created 02.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Elementor
 */

namespace IWP\Elementor;

use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Color;
use Elementor\Utils;
use Elementor\Widget_Base;

/**
 * TrainingBlock file class.
 */
class TrainingBlock extends Widget_Base {

	/**
	 * Get Name Widget.
	 *
	 * @inheritDoc
	 */
	public function get_name() {
		return __( 'trainingblock', 'iwp' );
	}

	/**
	 * Get Title.
	 *
	 * @return string|void
	 */
	public function get_title() {
		return __( 'Training Block', 'iwp' );
	}

	/**
	 * Get Icon Widget.
	 *
	 * @return string
	 */
	public function get_icon(): string {
		return 'eicon-table-of-contents';
	}

	/**
	 * Category Widget.
	 *
	 * @return string[]
	 */
	public function get_categories(): array {
		return [ 'basic' ];
	}

	/**
	 * Register controls.
	 */
	protected function _register_controls(): void {
		$this->start_controls_section(
			'content_training_block',
			[
				'label' => __( 'Content', 'iwp' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'tb_image',
			[
				'label'   => __( 'Choose Image', 'iwp' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'tb_title',
			[
				'label'       => __( 'Title', 'iwp' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'placeholder' => __( 'Type your title here', 'iwp' ),
			]
		);

		$this->add_control(
			'tb_sub_title',
			[
				'label'       => __( 'Sub-title', 'iwp' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'placeholder' => __( 'Type your sub-title here', 'iwp' ),
			]
		);

		$this->add_control(
			'as_link_button',
			[
				'label'         => __( 'Page link', 'iwp' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => __( 'https://your-link.com', 'iwp' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => true,
					'nofollow'    => true,
				],
			]
		);

		$this->add_control(
			'tb_text',
			[
				'type'        => Controls_Manager::WYSIWYG,
				'default'     => __( 'Default description', 'iwp' ),
				'placeholder' => __( 'Type your description here', 'iwp' ),
			]
		);

		$this->add_control(
			'tb_color_box',
			[
				'label'     => __( 'Color Box', 'iwp' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .title' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'tb_text_color_box',
			[
				'label'     => __( 'Text Color', 'iwp' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'tb_alignment',
			[
				'label'   => __( 'Alignment', 'plugin-domain' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'top'    => [
						'title' => __( 'Top', 'iwp' ),
						'icon'  => 'fa fa-align-top',
					],
					'button' => [
						'title' => __( 'Button', 'plugin-domain' ),
						'icon'  => 'fa fa-align-button',
					],
				],
				'default' => 'top',
				'toggle'  => true,
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Output html render.
	 */
	protected function render(): void {

		$settings = (object) $this->get_settings_for_display();
		?>
		<div class="img">
			<img
					src="<?php echo esc_url( $settings->tb_image['url'] ); ?>"
					alt="<?php get_the_title( $settings->tb_image['id'] ); ?>"
			>
			<div
					class="description <?php echo esc_attr( $settings->tb_alignment ); ?>"
			>
				<div class="head" style="background: <?php echo esc_attr( $settings->tb_color_box ); ?>">
					<h3 style="color:<?php echo esc_attr( $settings->tb_text_color_box ); ?>"><?php echo esc_html( $settings->tb_title ); ?></h3>
					<?php if ( ! empty( $settings->tb_sub_title ) ) : ?>
						<h5 style="color:<?php echo esc_attr( $settings->tb_text_color_box ); ?>"
							class="subtitle"><?php echo esc_html( $settings->tb_sub_title ); ?></h5>
					<?php endif; ?>
				</div>
				<?php if ( ! empty( $settings->tb_text ) ) : ?>
					<div class="hide-block">
						<div class="content"><?php echo wp_kses_post( $settings->tb_text ); ?></div>
					</div>
				<?php endif; ?>
			</div>
			<a
					href="<?php echo esc_url( $settings->as_link_button['url'] ); ?>"
					class="link"
					rel="<?php echo $settings->as_link_button['nofollow'] ? 'nofollow' : ''; ?>"
			></a>
		</div>
		<?php
	}

	/**
	 * Add Style Handler.
	 *
	 * @return array
	 */
	public function get_style_depends(): array {
		return [ 'style-handle' ];
	}
}

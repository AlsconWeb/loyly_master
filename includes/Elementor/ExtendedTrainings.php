<?php
/**
 * Created 03.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Elementor
 */

namespace IWP\Elementor;

use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Color;
use Elementor\Utils;
use Elementor\Widget_Base;

/**
 * ExtendedTrainings class file.
 */
class ExtendedTrainings extends Widget_Base {

	/**
	 * Get Name Widget.
	 *
	 * @inheritDoc
	 */
	public function get_name() {
		return __( 'extendedTrainings', 'iwp' );
	}

	/**
	 * Get Title.
	 *
	 * @return string|void
	 */
	public function get_title() {
		return __( 'Extended Trainings', 'iwp' );
	}

	/**
	 * Get Icon Widget.
	 *
	 * @return string
	 */
	public function get_icon(): string {
		return 'eicon-image-rollover';
	}

	/**
	 * Category Widget.
	 *
	 * @return string[]
	 */
	public function get_categories(): array {
		return [ 'basic' ];
	}

	/**
	 * Register controls.
	 */
	protected function _register_controls(): void {
		$this->start_controls_section(
			'content_extended_trainings',
			[
				'label' => __( 'Content', 'iwp' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'et_image',
			[
				'label'   => __( 'Choose Image', 'iwp' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'et_title',
			[
				'label'       => __( 'Title', 'iwp' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'placeholder' => __( 'Type your title here', 'iwp' ),
			]
		);

		$this->add_control(
			'et_sub_title',
			[
				'label'       => __( 'Sub-title', 'iwp' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'placeholder' => __( 'Type your sub-title here', 'iwp' ),
			]
		);

		$this->add_control(
			'et_link_button',
			[
				'label'         => __( 'Page link', 'iwp' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => __( 'https://your-link.com', 'iwp' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => true,
					'nofollow'    => true,
				],
			]
		);
		
		$this->add_control(
			'et_color_box',
			[
				'label'     => __( 'Color Box', 'iwp' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .title' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'et_text_color_box',
			[
				'label'     => __( 'Text Color', 'iwp' ),
				'type'      => Controls_Manager::COLOR,
				'scheme'    => [
					'type'  => Color::get_type(),
					'value' => Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .title' => 'color: {{VALUE}}',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Output html render.
	 */
	protected function render(): void {

		$settings = (object) $this->get_settings_for_display();
		?>
			<div class="img">
				<img
						src="<?php echo esc_url( $settings->et_image['url'] ); ?>"
						alt="<?php get_the_title( $settings->et_image['id'] ); ?>"
				>
				<div
						class="description"
						style="background: <?php echo esc_attr( $settings->et_color_box ); ?>">
					<div class="head">
						<h3 style="color:<?php echo esc_attr( $settings->et_text_color_box ); ?>"><?php echo wp_kses_post( $settings->et_title ); ?></h3>
						<?php if ( ! empty( $settings->et_sub_title ) ) : ?>
							<h5 style="color:<?php echo esc_attr( $settings->et_text_color_box ); ?>" class="subtitle"><?php echo esc_html( $settings->et_sub_title ); ?></h5>
						<?php endif; ?>
					</div>
				</div>
				<a
						href="<?php echo esc_url( $settings->et_link_button['url'] ); ?>"
						class="link"
						rel="<?php echo $settings->et_link_button['nofollow'] ? 'nofollow' : ''; ?>"
				></a>
			</div>
		<?php
	}

	/**
	 * Add Style Handler.
	 *
	 * @return array
	 */
	public function get_style_depends(): array {
		return [ 'style-handle' ];
	}
}

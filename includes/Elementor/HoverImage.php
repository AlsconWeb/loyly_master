<?php
/**
 * Created 02.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Elementor
 */

namespace IWP\Elementor;

use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Widget_Base;

/**
 * HoverImage class file.
 */
class HoverImage extends Widget_Base {

	/**
	 * Get Name Widget.
	 *
	 * @inheritDoc
	 */
	public function get_name() {
		return __( 'hoverimage', 'iwp' );
	}

	/**
	 * Get Title.
	 *
	 * @return string|void
	 */
	public function get_title() {
		return __( 'Hover Image', 'iwp' );
	}

	/**
	 * Get Icon Widget.
	 *
	 * @return string
	 */
	public function get_icon(): string {
		return 'eicon-image-rollover';
	}

	/**
	 * Category Widget.
	 *
	 * @return string[]
	 */
	public function get_categories(): array {
		return [ 'basic' ];
	}

	/**
	 * Register controls.
	 */
	protected function _register_controls(): void {
		$this->start_controls_section(
			'content_hover_image',
			[
				'label' => __( 'Content', 'iwp' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'image',
			[
				'label'   => __( 'Choose Image', 'iwp' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'as_title',
			[
				'label'       => __( 'Title', 'iwp' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'placeholder' => __( 'Type your title here', 'iwp' ),
			]
		);

		$this->add_control(
			'as_link_button',
			[
				'label'         => __( 'Page link', 'iwp' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => __( 'https://your-link.com', 'iwp' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => true,
					'nofollow'    => true,
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Output html render.
	 */
	protected function render(): void {

		$settings = (object) $this->get_settings_for_display();
		?>
		<div class="image">
			<div class="description">
				<h3><?php echo esc_html( $settings->as_title ); ?></h3>
			</div>
			<img
					src="<?php echo esc_url( $settings->image['url'] ); ?>"
					alt="<?php get_the_title( $settings->image['id'] ); ?>"
			>
			<a
					href="<?php echo esc_url( $settings->as_link_button['url'] ); ?>"
					class="link"
					rel="<?php echo $settings->as_link_button['nofollow'] ? 'nofollow' : ''; ?>"
			></a>
		</div>
		<?php
	}

	/**
	 * Add Style Handler.
	 *
	 * @return array
	 */
	public function get_style_depends(): array {
		return [ 'style-handle' ];
	}
}

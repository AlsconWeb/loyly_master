<?php
/**
 * Created 02.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Elementor
 */

namespace IWP\Elementor;

use Elementor\Plugin;

/**
 * RegisterWidgets class file.
 */
class RegisterWidgetsElementary {
	/**
	 * Instance
	 *
	 * @var null
	 */
	protected static $instance;

	/**
	 * Constructor RegisterWidgets.
	 */
	protected function __construct() {
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'register_widgets' ] );
	}

	/**
	 * Get instance widget.
	 *
	 * @return null
	 */
	public static function get_instance() {
		if ( ! isset( static::$instance ) ) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * Register Widget.
	 */
	public function register_widgets(): void {
		Plugin::instance()->widgets_manager->register_widget_type( new AboutSpa() );
		Plugin::instance()->widgets_manager->register_widget_type( new HoverImage() );
		Plugin::instance()->widgets_manager->register_widget_type( new TrainingBlock() );
		Plugin::instance()->widgets_manager->register_widget_type( new TestimonialsSlider() );
	}
}

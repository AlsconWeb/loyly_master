<?php
/**
 * Created 06.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP\Elementor
 */

namespace IWP\Elementor;

use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Widget_Base;

/**
 * TestimonialsSlider class file.
 */
class TestimonialsSlider extends Widget_Base {

	/**
	 * Get Name Widget.
	 *
	 * @inheritDoc
	 */
	public function get_name() {
		return __( 'testimonialsSlider', 'iwp' );
	}

	/**
	 * Get Title.
	 *
	 * @return string|void
	 */
	public function get_title() {
		return __( 'Testimonials Slider', 'iwp' );
	}

	/**
	 * Get Icon Widget.
	 *
	 * @return string
	 */
	public function get_icon(): string {
		return 'eicon-thumbnails-down';
	}

	/**
	 * Category Widget.
	 *
	 * @return string[]
	 */
	public function get_categories(): array {
		return [ 'basic' ];
	}

	/**
	 * Register controls.
	 */
	protected function _register_controls(): void {

		$repeater = new Repeater();

		$this->start_controls_section(
			'content_testimonials_slider',
			[
				'label' => __( 'Content', 'iwp' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$repeater->add_control(
			'testimonials_name',
			[
				'label'       => __( 'Name and Surname', 'iwp' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'placeholder' => __( 'Type Name and Surname here', 'iwp' ),
			]
		);

		$repeater->add_control(
			'testimonials_text',
			[
				'label'       => __( 'Text', 'iwp' ),
				'type'        => Controls_Manager::WYSIWYG,
				'default'     => '',
				'placeholder' => __( 'Type your testimonials text here', 'iwp' ),
			]
		);

		$repeater->add_control(
			'testimonials_position',
			[
				'label'       => __( 'Position', 'iwp' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'placeholder' => __( 'Type position here', 'iwp' ),
			]
		);

		$repeater->add_control(
			'testimonials_avatar',
			[
				'label'   => __( 'Choose Image', 'iwp' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'testimonials',
			[
				'label'   => __( 'Testimonials List', 'iwp' ),
				'type'    => Controls_Manager::REPEATER,
				'fields'  => $repeater->get_controls(),
				'default' => [],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Output html render.
	 */
	protected function render(): void {

		$settings = (object) $this->get_settings_for_display();
		?>
		<div class="testimonials-slider">
			<?php if ( $settings->testimonials ) : ?>
				<?php foreach ( $settings->testimonials as $item ) : ?>
					<div class="item">
						<img
								src="<?php echo esc_url( $item['testimonials_avatar']['url'] ); ?>"
								alt="<?php get_the_title( $item['testimonials_avatar']['id'] ); ?>"
						>
						<div class="description">
							<h3 class="title"><?php echo esc_html( $item['testimonials_name'] ); ?></h3>
							<?php echo wp_kses_post( wpautop( $item['testimonials_text'] ) ); ?>
							<h5 class="position"><?php echo esc_html( $item['testimonials_position'] ); ?></h5>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<?php
	}

	/**
	 * Add Style Handler.
	 *
	 * @return array
	 */
	public function get_style_depends(): array {
		return [ 'style-handle' ];
	}
}

<?php
/**
 * Created 02.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP
 */

$currentYear = current_time( 'Y' );
$copyright   = (string) get_theme_mod( 'loyly_copyright' );
?>
<!-- footer -->
<footer class="wow">
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 text-center">
					<span class="text-small text-uppercase letter-spacing-1 alt-font">
						<?php
						if ( $copyright ) :
							echo wp_kses( $copyright, 'post' );
							?>
						<?php else : ?>
							All Rights Reserved © 2021
							<a href="https://aromen.be/" target="_blank">Aromen BVBA.</a>code create By
							<a href="https://i-wp-dev.com/" target="_blank">Alex L</a>
						<?php endif; ?>
					</span>
					<ul class="elementor-icon-list-items elementor-inline-items">
						<li class="elementor-icon-list-item elementor-inline-item">
							<a href="#">

								<span class="elementor-icon-list-icon">
									<i aria-hidden="true" class="fab fa-facebook-f"></i> </span>
								<span class="elementor-icon-list-text"></span>
							</a>
						</li>
						<li class="elementor-icon-list-item elementor-inline-item">
							<a href="#">

								<span class="elementor-icon-list-icon">
									<i aria-hidden="true" class="fab fa-instagram"></i> </span>
								<span class="elementor-icon-list-text"></span>
							</a>
						</li>
						<li class="elementor-icon-list-item elementor-inline-item">
							<a href="#">

								<span class="elementor-icon-list-icon">
									<i aria-hidden="true" class="fab fa-linkedin-in"></i> </span>
								<span class="elementor-icon-list-text"></span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- end footer -->
<!-- scroll to top -->
<a href="javascript:;" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top -->
<?php wp_footer(); ?>
</body>
</html>

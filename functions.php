<?php
/**
 * Created 02.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package IWP
 */

use IWP\ThemeInit;

define( 'IWP_THEME_DIR', get_template_directory() );
define( 'IWP_THEME_URL', get_template_directory_uri() );

// add autoload class composer PSR-4.
require_once IWP_THEME_DIR . '/vendor/autoload.php';

global $loylaThemes;

$loylaThemes = new ThemeInit();
add_filter( 'upload_mimes', 'svg_upload_allow' );

# Добавляет SVG в список разрешенных для загрузки файлов.
function svg_upload_allow( $mimes ) {
	$mimes['svg']  = 'image/svg+xml';

	return $mimes;
}